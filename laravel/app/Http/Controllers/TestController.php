<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function input(Request $request)
    {
        $name=$request->input('name');
        return'name值为' . $name;
    }
}